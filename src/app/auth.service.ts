/**
 * Created by astargh on 26.08.17.
 */

import { Injectable } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { UserDataService } from './userdata.service';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/share';

@Injectable()

export class AuthService {

  private ssoKey: string = 'some_key_for_auth';
  private userData;
  private userKey;

  constructor( private activatedRoute: ActivatedRoute,
               private _userDataService: UserDataService) {
    this.activatedRoute.queryParams.subscribe((params: Params) => {
      if (params.hasOwnProperty('sig')) {
        let sig = params['sig'];
        let ssoReturn = params['sso'];
        ssoReturn = ssoReturn.replace('/x[0-9A-F][0-9A-F]/g', '');
        let ssoDecrypt = atob(ssoReturn);
        ssoDecrypt = this.queryStringToJSON(ssoDecrypt);
        this.userData = ssoDecrypt;
        localStorage.setItem('username', ssoDecrypt['username']);
        this._userDataService.UserData.api_username = ssoDecrypt['username'];
        console.log(this._userDataService.UserData);
      }
    });
  }

  private queryStringToJSON(string: string) {
    let pairs = (string + '').split('&');
    let result = {};
    pairs.forEach(function (pair) {
      let NewPair = pair.split('=');
      result[NewPair[0]] = decodeURIComponent(NewPair[1] || '');
    });

    return JSON.parse(JSON.stringify(result));
  }

}
