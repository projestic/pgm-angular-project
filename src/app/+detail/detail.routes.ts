import { DetailComponent } from './detail.component';
import { TopicComponent } from '../topic/topic.component';

export const routes = [
  { path: '', children: [
    { path: '', component: DetailComponent },
    {  path: 'topic/:id',
      data: {
        breadcrumb: 'Topic'
      },
      component: TopicComponent, }
  ]},
];
