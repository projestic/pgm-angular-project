import { Injectable } from '@angular/core';
import { RequestOptions, Headers, Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { CreateTopic } from './createTopic';

@Injectable()
export class ModalService {
  constructor(public http: Http) {}


  makeAjaxRequest( userdata ) {
    return this.http.get(userdata.host + 'session/current.json?api_key=' + userdata.api_key + '&api_username=' + userdata.api_username)
      .map(res => res.json());
  }

  logOutRequest(userID, userdata) {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    let message = {
      "api_key": userdata.api_key,
      "api_username": userdata.api_username,
      "success": 'ok'
    };
    return this.http.post(userdata.host + 'admin/users/' + userID + '/log_out.json?api_key=' + userdata.api_key + '&api_username=' + userdata.api_username, JSON.stringify(message))
      .map(res => res.json());
  }


  sendPostMessage(message: CreateTopic, userdata): Observable<CreateTopic> {
    return Observable.fromPromise(new Promise((resolve, reject) => {
      let xhr = new XMLHttpRequest();
      let data = new FormData();
      data.append("api_key", userdata.api_key);
      data.append("api_username", userdata.api_username);
      data.append("title", message.title);
      data.append("raw", message.raw);
      data.append("category", message.category['id']);

      xhr.onreadystatechange = function() {
        if (xhr.readyState === 4) {
          if (xhr.status === 200) {
            resolve(JSON.parse(xhr.response));
          } else {
            reject(xhr.response);
          }
        }
      }
      xhr.open("POST",userdata.host + 'posts', true);
      xhr.send(data);
    }));
  }


}
