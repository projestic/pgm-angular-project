export class CreateTopic {
  public title: string;
  public raw: string;
  public category = {
    id: '',
    name: '',
  };
}
