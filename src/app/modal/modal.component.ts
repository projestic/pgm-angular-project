import { Component, OnInit } from '@angular/core';
import { ModalService } from './modal.service';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { Router, ActivatedRoute, Params, NavigationExtras, NavigationEnd } from '@angular/router';
import { HomeService } from '../home/home.service';
import { CreateTopic } from './createTopic';
import { LoginService } from '../login.service';
import { AuthService } from '../auth.service';
import { UserDataService } from '../userdata.service';

@Component({
  selector: 'modal-block',
  providers: [
    ModalService,
    HomeService
  ],
  styleUrls: ['./modal.component.scss'],
  templateUrl: './modal.component.html',
})

export class ModalComponent implements OnInit {

  public closeResult: string;
  public userData;
  public userDataModal;
  public getData;
  public sendPostMessage;
  public newTopicMessage: CreateTopic = {
    title: '',
    raw: '',
    category: {
      id: '',
      name: ''
    }
  }

  constructor(private _httpService: ModalService,
              private modalService: NgbModal,
              private HomeService: HomeService,
              private _loginService: LoginService,
              public activatedRoute: ActivatedRoute,
              private router: Router,
              private _userDataService: UserDataService ) {
    this.activatedRoute.queryParams.subscribe((params: Params) => {
      this.userData = this._userDataService.getUserData();
      console.log(this.userData.api_username);
      if (localStorage.getItem('username') != null) {
        this.getCategoryList(this.userData);
        this.currentUser(this.userData);
      }
    });
  };

  ngOnInit () {}

  logOut (userID) {
    let id = userID;
    this._httpService.logOutRequest(id, this.userData).subscribe(
      data => {
        localStorage.removeItem('username');
        this.userData = {};
        window.location.reload();
      }
    );
  }

  open(content) {
    this.modalService.open(content).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  getCategoryList(userdata) {
    this.HomeService.getAjaxObject(userdata).subscribe(
      data => {
        this.getData = data.category_list.categories;
      }
    );
  }

  onSubmit(event, username, category, title) {
    event.preventDefault();
    this._httpService.sendPostMessage(this.newTopicMessage, username).subscribe(
      data => {
        this.sendPostMessage = data;
        let navigationExtras: NavigationExtras = {
          queryParams: {
            'id': this.sendPostMessage.topic_id,
            'category': category
          }
        };

        this.router.navigate(['/topic/' , title ], navigationExtras);
      }
    );

  }

  LoginInHeader() {
    this._loginService.LoginIn();
  }

  currentUser(userdata) {
    this._httpService.makeAjaxRequest(userdata).subscribe(
      data => {
        if ( data == null) {
          console.log('null');
        } else {
          this.userDataModal = data;
        }

      }
    );
  }
}
