import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'footer-block',
    styleUrls: ['./footer.component.scss'],
    templateUrl: 'footer.component.html'
})
export class footerComponent implements OnInit {
    constructor() {}

    ngOnInit(){
        console.log('hello `Footer` component');
    }
}