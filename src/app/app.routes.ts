import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home';
import { UserpageComponent } from './userpage';
import { CategoryComponent } from './category';
import { LatestComponent } from './latest';
import { TopicComponent } from './topic';
import { NoContentComponent } from './no-content';

const routes: Routes = [
  {
    path: '',
    children: [
      { path: '',
        component: HomeComponent
      },
      { path: ':/id',
        component: HomeComponent
      }
    ]
  },
  { path: 'user/:id',
    data: {
      breadcrumb: 'User'
    },
    children: [
      {
        path: '',
        component: UserpageComponent,
        pathMatch: 'full'
      },
      { path: 'topic/:id',
        component: TopicComponent,
        data: {
          breadcrumb: 'Topic'
        }
      }
    ]
  },
  {
    path: 'category/:id',
    data: {
      breadcrumb: 'Category'
    },
    children: [
      {
        path: '',
        component: CategoryComponent,
        pathMatch: 'full'
      },
      { path: 'topic/:id',
        component: TopicComponent,
        data: {
          breadcrumb: 'Topic'
        }
      }
    ]
  },
  { path: 'latest',
    data: {
      breadcrumb: 'Latest'
    },
    children: [
      {
        path: '',
        component: LatestComponent,
        pathMatch: 'full'
      },
      { path: 'topic/:id',
        component: TopicComponent,
        data: {
          breadcrumb: 'Topic'
        }
      }
    ]
  },
  { path: 'topic/:id',
    component: TopicComponent,
  },
  { path: '**',    component: NoContentComponent }
];

export const ROUTES: ModuleWithProviders = RouterModule.forRoot(routes);


