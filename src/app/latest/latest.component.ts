import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LatestService } from './latest.service';
import { ModalService } from '../modal/modal.service';
import { UserDataService } from '../userdata.service';

export class FilterList {
    title: string;
    bookmarked: boolean;
    filterTitle: string;
    filterTitle2: string;
}

@Component({
  selector: 'category',
  providers: [ LatestService, ModalService ],
  styleUrls: [`./latest.component.scss`],
  templateUrl: 'latest.component.html',
})

export class LatestComponent implements OnInit {

  public departmentId;
  public apiLink;
  public infoName;
  public infoSlug;
  public infoImg;
  public infoId;
  public filterTitle;
  public filterTitle2;
  public filterargs: object;
  public userData;
  public newDataCat;
  public userList: Array<object> = [];

  selectedItem;
  constructor(private _httpService: LatestService,
              private route: ActivatedRoute,
              private ModalService: ModalService,
              private _userData: UserDataService) {}

  getData: object;
  getDataCat: object;
  infos: any;


  currentUser(userdata) {
    this.ModalService.makeAjaxRequest(userdata).subscribe(
      data => {
        if ( data == null) {
          console.log('null');
        } else {
          this.userData = data;

        }

      }
    );
  }



  getTopicList( userdata ) {
    this.apiLink = userdata.host + "latest.json?ascending=false&order=activity&api_key=" + userdata.api_key + "&api_username="  + userdata.api_username;
    this._httpService.getAjaxObjectCat( this.apiLink ).subscribe(
      data => {
        this.getDataCat = data;

        for (let userItem of this.getDataCat['users'] ) {
          this._httpService.getUserListRequest(userdata, userItem).subscribe(
            data => {
              this.userList.push(data);
            }
          );
        }

      }
    );
  }


  ngOnInit() {

    this.userData = this._userData.getUserData();

    this.currentUser( this.userData );

    this.getTopicList( this.userData );

    let id = this.route.snapshot.params['id'];

    this.departmentId = id;

    this.filterTitle = 'category_id';

    /*this.getUserList();*/
  }

  /*getUserList() {
    this._httpService.getUserListRequest(this.userData).subscribe(
      data => {
        this.userList = data;
        console.log(this.userList);
      }
    );
  }*/

}

