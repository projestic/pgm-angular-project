/**
 * Created by Astargh on 12.07.2017.
 */


import { Pipe, PipeTransform } from '@angular/core';
import {isBoolean} from "util";

@Pipe({
    name: 'myfilter',
    pure: false
})
export class MyFilterPipe implements PipeTransform {
    transform(items: any[], filter: Object): any {
        if (!items || !filter) {
            return items;
        }
        if (filter['bookmarked'] == false){
            return items;
        } else {
            return items.filter( item =>  item.bookmarked == filter['bookmarked'] );
        }

    }


}