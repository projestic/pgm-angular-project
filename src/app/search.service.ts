/**
 * Created by Astargh on 12.07.2017.
 */

import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { UserDataService} from './userdata.service';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';

@Injectable()
export class SearchService {
    queryUrl: string = 'term=';
    constructor(private http: Http,
                private _userData: UserDataService) { }

    baseUrl: string = this._userData.UserData.host + 'search/query.json?';

    search(terms: Observable<string>) {
        return terms.debounceTime(400)
            .distinctUntilChanged()
            .switchMap( term => this.searchEntries(term) );
    }

    searchEntries(term) {
        console.log(term);
        if ( term !== '' ) {
            return this.http
                .get( this.baseUrl + this.queryUrl + term + '&include_blurbs=true&api_key=' + this._userData.UserData.api_key + '&api_username=' + this._userData.UserData.api_username)
                .map(res => res.json());
        } else {
            return this.http
                .get( this.baseUrl + this.queryUrl + '_' + '&include_blurbs=true&api_key=' + this._userData.UserData.api_key + '&api_username=' + this._userData.UserData.api_username)
                .map(res => res.json());
        }

    }
}
