/**
 * Angular 2 decorators and services
 */
import { Component, OnInit, NgModule, OnChanges } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { headerComponent } from './header/index';
import { breadcrumbsComponent } from './breadcrumbs/index';
import { footerComponent } from './footer/footer.component';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { AuthService } from './auth.service';


/**
 * App Component
 * Top Level Component
 */
@Component({
  selector: 'app-root',
  styleUrls: ['./app.component.scss'],
  templateUrl: 'app.component.html',
})
@NgModule({
  imports:      [ BrowserModule ],
  declarations: [ AppComponent, headerComponent, breadcrumbsComponent, footerComponent ],
  bootstrap:    [ AppComponent ]
})
export class AppComponent implements OnInit, OnChanges {

  constructor(private _authService: AuthService,
              public http: Http ) {}


  public ngOnChanges() {

  }
  public ngOnInit() {
    
  }
}

/**
 * Please review the https://github.com/AngularClass/angular2-examples/ repo for
 * more angular app examples that you may copy/paste
 * (The examples may not be updated as quickly. Please open an issue on github for us to update it)
 * For help or questions please contact us at @AngularClass on twitter
 * or our chat on Slack at https://AngularClass.com/slack-join
 */
