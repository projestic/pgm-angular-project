import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {
  NgModule,
  ApplicationRef
} from '@angular/core';
import {
  removeNgStyles,
  createNewHosts,
  createInputTransfer
} from '@angularclass/hmr';
import { ENV_PROVIDERS } from './environment';
import { ROUTES } from './app.routes';
import { AppComponent } from './app.component';
import { APP_RESOLVER_PROVIDERS } from './app.resolver';
import { AppState, InternalStateType } from './app.service';
import { AuthService } from './auth.service';
import { UserDataService } from './userdata.service';
import { HomeComponent } from './home';
import { AboutComponent } from './about';
import { NoContentComponent } from './no-content';
import { headerComponent } from './header/header.component';
import { breadcrumbsComponent } from './breadcrumbs/breadcrumbs.component';
import { ModalComponent } from './modal/modal.component';
import { footerComponent } from './footer/footer.component';
import { CategoryComponent } from './category/category.component';
import { OrderBy } from './orderBy';
import { TimeAgoPipe } from './timeAgo.pipe';
import { MyFilterPipe } from './filter';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { SearchService } from './search.service';
import { TopicComponent } from './topic/topic.component';
import { LatestComponent } from './latest/latest.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CeiboShare } from 'ng2-social-share';
import { NouisliderModule } from 'ng2-nouislider';
import { TinymceModule } from 'angular2-tinymce';
import '../styles/styles.scss';
import { LoginService } from './login.service';
import { UserpageComponent } from './userpage/userpage.component';
import { InfiniteScrollModule } from 'angular2-infinite-scroll';
import { NguiInfiniteListModule } from '@ngui/infinite-list';

// Application wide providers
const APP_PROVIDERS = [
  ...APP_RESOLVER_PROVIDERS,
  AppState
];

type StoreType = {
  state: InternalStateType,
  restoreInputValues: () => void,
  disposeOldHosts: () => void
};

/**
 * `AppModule` is the main entry point into Angular2's bootstraping process
 */
@NgModule({
  bootstrap: [ AppComponent ],
  declarations: [
    AppComponent,
    AboutComponent,
    HomeComponent,
    NoContentComponent,
    headerComponent,
    breadcrumbsComponent,
    ModalComponent,
    footerComponent,
    CategoryComponent,
    OrderBy,
    MyFilterPipe,
    TopicComponent,
    TimeAgoPipe,
    LatestComponent,
    CeiboShare,
    UserpageComponent
  ],
  /**
   * Import Angular's modules.
   */
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    ROUTES,
    NgbModule.forRoot(),
    NouisliderModule,
    TinymceModule.withConfig({
      selector: 'textarea',
      menubar: false,
      toolbar: 'insert | undo redo |  styleselect | bold italic backcolor  | bullist numlist outdent indent | removeformat | help',
      statusbar: false,
      height: 270,
      max_height: 270,
    }),
    InfiniteScrollModule,
    NguiInfiniteListModule
  ],
  /**
   * Expose our Services and Providers into Angular's dependency injection.
   */
  providers: [
    ENV_PROVIDERS,
    APP_PROVIDERS,
    AuthService,
    UserDataService,
    LoginService,
    OrderBy,
    {provide: LocationStrategy, useClass: HashLocationStrategy},
    SearchService
  ]
})
export class AppModule {

  constructor(
    public appRef: ApplicationRef,
    public appState: AppState
  ) {}

  public hmrOnInit(store: StoreType) {
    if (!store || !store.state) {
      return;
    }
    console.log('HMR store', JSON.stringify(store, null, 2));
    /**
     * Set state
     */
    this.appState._state = store.state;
    /**
     * Set input values
     */
    if ('restoreInputValues' in store) {
      let restoreInputValues = store.restoreInputValues;
      setTimeout(restoreInputValues);
    }

    this.appRef.tick();
    delete store.state;
    delete store.restoreInputValues;
  }

  public hmrOnDestroy(store: StoreType) {
    const cmpLocation = this.appRef.components.map((cmp) => cmp.location.nativeElement);
    /**
     * Save state
     */
    const state = this.appState._state;
    store.state = state;
    /**
     * Recreate root elements
     */
    store.disposeOldHosts = createNewHosts(cmpLocation);
    /**
     * Save input values
     */
    store.restoreInputValues  = createInputTransfer();
    /**
     * Remove styles
     */
    removeNgStyles();
  }

  public hmrAfterDestroy(store: StoreType) {
    /**
     * Display new elements
     */
    store.disposeOldHosts();
    delete store.disposeOldHosts;
  }

}
