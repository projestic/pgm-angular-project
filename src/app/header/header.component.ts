import { Component, OnInit, NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SearchService } from '../search.service';
import { Subject } from 'rxjs/Subject';
import { Router, NavigationExtras } from '@angular/router';

@Component({
    selector: 'header-block',
    styleUrls: ['./header.component.scss'],
    templateUrl: 'header.component.html',
    providers: [SearchService]
})

@NgModule({
    imports: [NgbModule.forRoot()]
})

export class headerComponent implements OnInit {
    public angularclassLogo = 'assets/img/header-logo.png';
    public url = '/';
    results: object;
    searchTerm$ = new Subject<string>();

    inputValue: string = '';

    constructor(private searchService: SearchService,
                private router: Router) {
        this.searchService.search(this.searchTerm$)
            .subscribe( data => {
                console.log(data);
                this.results = data;
            });
    }

    ngOnInit() {

    }

    resetForm(event, link, id) {
      event.preventDefault();
      this.inputValue = '';
      this.results = {};
      console.log("form comlete");
      let navigationExtras: NavigationExtras = {
        queryParams: { id: id }
      };
      this.router.navigate(['/topic/' , link], navigationExtras);
    }
}
