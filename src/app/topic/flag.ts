class Flag {
  public id: string;
  public title: string;
}

export const FLAG: Array<Flag> = [
  {
    id: '3',
    title: 'Off-Topic'
  },
  {
    id: '4',
    title: 'Inappropriate'
  },
  {
    id: '8',
    title: 'Spam'
  },
  {
    id: '7',
    title: 'Custom Message'
  }
]
