/**
 * Created by Astargh on 06.07.2017.
 */

import { Injectable } from '@angular/core';
import { RequestOptions, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import { Message } from './message';
import { User } from '../user';
import { CategoryService } from '../category/category.service';

@Injectable()
export class TopicService extends CategoryService {

    getAjaxObjectTopics( user: User, topicId ) {
      return this.http.get(user.host + 't/' + topicId + '.json?api_key=' + user.api_key + '&api_username=' + user.api_username)
        .map(res => res.json());
    }

    getAjaxObjectPosts( postId, user: User, topicId ) {

      return this.http.get(user.host + 't/' + topicId + '/posts.json?api_key=' + user.api_key + '&api_username=' + user.api_username + postId)
        .map(res => res.json());
    }

    sendPostMessage(message: Message, user: User): Observable<Message>  {
      return Observable.fromPromise(new Promise((resolve, reject) => {
        let xhr = new XMLHttpRequest();
        let data = new FormData();
        data.append("api_key", user.api_key);
        data.append("api_username", user.api_username);
        data.append("topic_id", message.topic_id);
        data.append("raw", message.raw);

        xhr.onreadystatechange = function() {
          if (xhr.readyState === 4) {
            if (xhr.status === 200) {
              resolve(JSON.parse(xhr.response));
            } else {
              reject(xhr.response);
            }
          }
        }
        xhr.open("POST",user.host + 'posts', true);
        xhr.send(data);
      }));
    }

    sendPutMessage(postId, message, user: User): Observable<Message>  {
      return Observable.fromPromise(new Promise((resolve, reject) => {
        let xhr = new XMLHttpRequest();
        let data = new FormData();
        data.append("api_key", user.api_key);
        data.append("api_username", user.api_username);
        data.append("post[raw]", message);

        xhr.onreadystatechange = function() {
          if (xhr.readyState === 4) {
            if (xhr.status === 200) {
              resolve(JSON.parse(xhr.response));
            } else {
              reject(xhr.response);
            }
          }
        }
        xhr.open("PUT",'http://cors.projestic.com:8888/' + user.host + 'posts/' + postId );
        xhr.send(data);
      }));
    }

    sendPostLike(user, sendPostLike) {
        return this.http.post(user.host + 'post_actions' , sendPostLike)
            .map(this.extractData)
            .catch(this.handleErrorObservable);

    }

    sendPostUnLike(user, topicId, topicActionsId) {
      return Observable.fromPromise(new Promise((resolve, reject) => {
        let xhr = new XMLHttpRequest();

        let data = new FormData();
        data.append('api_key'  , user.api_key);
        data.append("api_username", user.api_username);
        data.append("post_action_type_id", topicActionsId);



        xhr.onreadystatechange = function() {
          if (xhr.readyState === 4) {
            if (xhr.status === 200) {
              resolve(JSON.parse(xhr.response));
            } else {
              reject(xhr.response);
            }
          }
        };

        xhr.open('DELETE','http://cors.projestic.com:8888/' + user.host + 'post_actions/' + topicId);
        xhr.setRequestHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
        xhr.setRequestHeader('Access-Control-Allow-Origin', '*');
        xhr.setRequestHeader('Access-Control-Allow-Headers', 'Content-Type')
        xhr.send(data);
      }));
    }

    private extractData(res: Response) {
        let body = res.json();
        console.log(res.json());
        return body.data || {};
    }
    private handleErrorObservable (error: Response | any) {
        console.error(error.message || error);
        return Observable.throw(error.message || error);
    }
}

