import {
  Component,
  OnInit,
  ChangeDetectorRef,
  ElementRef,
  HostListener
} from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import 'rxjs/add/operator/filter';
import { TopicService } from './topic.service';
import { Message } from './message';
import { User } from '../user';
import { ModalService } from '../modal/modal.service';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FLAG } from './flag';
import { LoginService } from '../login.service';
import { UserDataService } from '../userdata.service';

@Component({
  selector: 'topic',
  providers: [TopicService, ModalService],
  styleUrls: [`./topic.component.scss`, './nouislider.scss'],
  templateUrl: 'topic.component.html'
})

export class TopicComponent implements OnInit {

  public editFormRaw;
  public closeResult: string;
  public getDataTopic;
  public sendPostMessage: object;
  public getDataTopicPosts: object;
  public postId;
  public topicId;
  public topicCategory;
  public classIndex;
  public getData: object;
  public coords;
  public topicIdent;
  public showForm: boolean = false;
  public editForm: boolean = false;
  public coordsBlock = {
    top: '0',
    left: '0'
  };
  public sendPostLike;
  public sendPostUnLike;
  public textHighlight;
  public title;
  public message: Message = {
    topic_id: '',
    raw: ''
  };
  public user: User;
  public actionSummaryId: string;
  public postEditId;
  public postIdFlag;
  public Flag = FLAG;
  public postLink;
  public error;
  public fragment;
  public maxValue = 5;
  public sliderConfig = {
    orientation: 'vertical'
  };
  public wid;
  public someRange;
  public SelectValue;
  public top = 0;
  public showScroll = false;
  public previewVisible = true;
  public testName;

  constructor(private _httpService: TopicService,
              private router: Router,
              private modalService: NgbModal,
              private activatedRoute: ActivatedRoute,
              private ref: ChangeDetectorRef,
              private elRef: ElementRef,
              private _loginService: LoginService,
              private _userData: UserDataService,) {
    this.wid = 'w1';
    this.SelectValue = '1';
  }

  @HostListener("window:scroll", [])
  onWindowScroll() {
    if (this.maxValue > 1) {
      const offset = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;
      let hElement: HTMLElement = this.elRef.nativeElement;
      /*this.SelectValue = this.wid.substr(1);*/
      let scrollBlock = document.getElementById('topic-list').offsetTop;

      let d = hElement.getElementsByClassName('scroller');

      if ( offset > 200) {

        d[0]['style'].position = 'fixed';
        d[0]['style'].top = '10px';
      } else {
        d[0]['style'].position = 'absolute';
        d[0]['style'].top = '-90px';
      }
      for ( let i = 1; i < (this.maxValue + 1); i++) {
        let scrollBlockOT = document.getElementById('w' + i).offsetTop;
        let height = document.getElementById('w' + i).offsetHeight;
        if (offset + (window.innerHeight / 2) > (scrollBlockOT + 235) && offset + (window.innerHeight / 2) < (scrollBlockOT + height + 235)) {
          this.SelectValue = i;
          this.top = Number(this.SelectValue - 1) / (Number(this.maxValue) - 1 ) * 100;
          d[0]['style'].display = 'block';
        } else if (offset > document.getElementById('w' + this.maxValue).offsetTop) {
          this.SelectValue = this.maxValue;
          this.top = 100;
          d[0]['style'].display = 'none';
        }
      }
    }

  }

  public ngOnInit() {
    this.testName = localStorage.getItem('username')

    this.router.events.filter(event => event instanceof NavigationEnd).subscribe((event) => {
      this.topicId = this.activatedRoute.snapshot.queryParams['id'];
      this.user = this._userData.getUserData();
      console.log(this.user);
      this.TopicLoader(this.user, this.topicId);


      const tree = this.router.parseUrl(this.router.url);
      if (tree.fragment) {
        const element = document.querySelector("#" + tree.fragment);
        if (element) {
          element.scrollIntoView(element);
        }
      }
    });

    this.activatedRoute.fragment.subscribe(fragment => {
      this.fragment = fragment;
    });

    setTimeout(
      () => {
        try {
          document.querySelector('#' + this.fragment).scrollIntoView();
        } catch (e) {}
      }, 1000
    );

  }

  getCategoryList(postdata) {
    this._httpService.getAjaxObject(this.user).subscribe(
      data => {
        this.getData = data.category_list.categories;
        this.findCategoryImg(this.getData, postdata);
      }
    );
  }

  private findCategoryImg(catObj, postObj) {
    for (let category of catObj) {
      if (category.id == postObj.category_id) {
        this.topicCategory = category;
        console.log(this.topicCategory);
      }
    }
  }

  private TopicLoader(user, topicId) {
    this._httpService.getAjaxObjectTopics(user, topicId).subscribe(
      (data) => {
        this.getDataTopic = data;
        this.message.topic_id = this.getDataTopic.id;
        console.log(this.getDataTopic);
        this.maxValue = this.getDataTopic['post_stream']['stream'].length;
        this.postId = this.postId || [];
        for (let post of this.getDataTopic['post_stream'].stream) {
          this.postId = this.postId + '&post_ids%5B%5D=' + post;
        }
        this.getCategoryList(this.getDataTopic);
        this._httpService.getAjaxObjectPosts(this.postId, this.user, this.topicId).subscribe(
          (dataSub) => {
            this.getDataTopicPosts = dataSub;
            console.log(this.getDataTopicPosts);
          }
        );
      },
      err => {
        this.error = err;
        console.log(this.error);
        this.router.navigate(['']);
      });
  }

  public onSubmit(event) {
    event.preventDefault();
    this._httpService.sendPostMessage(this.message, this.user).subscribe(
      (data) => {
        this.sendPostMessage = data;
        this.TopicLoader(this.user, this.topicId);
      }
    );

  }

  public onReply(event, contentMessage, contentTitle) {
    event.preventDefault();
    this.showForm = true;
  }

  public closeForm(event) {
    event.preventDefault();
    this.showForm = false;
    this.editForm = false;
  }

  onEdit(event, topicId, text) {
    event.preventDefault();
    this.editFormRaw = text;
    this.postEditId = topicId;
    this.editForm = true;
  }

  onEditSubmit(event) {
    event.preventDefault();
    this._httpService.sendPutMessage(this.postEditId, this.editFormRaw, this.user).subscribe(
      (data) => {
        this.sendPostMessage = data;
        console.log(this.topicId);
        this.TopicLoader(this.user, this.topicId);
      }
    );
  }

  public onQuote(event, contentMessage, contentTitle, postId, topicId, topicImg) {
    event.preventDefault();
    event.target.classList.remove('quote-wrapper');
    window.getSelection().removeAllRanges();
    let messageText = '[quote="' + contentTitle + ', post:' + postId + ', topic:' + postId + '"]' + contentMessage + '[/quote]';
    this.message.raw = this.message.raw + messageText;
  }

  public onLike(event, topicId, topicActionsId) {
    event.preventDefault();

    let data = new FormData();
    data.append("api_key", this.user.api_key);
    data.append("api_username", this.user.api_username);
    data.append("id", topicId);
    data.append("post_action_type_id", topicActionsId);
    this._httpService.sendPostLike(this.user, data).subscribe(
      (data) => {
        this.sendPostLike = data;
        this.TopicLoader(this.user, this.topicId);
      }
    );
  }

  onFlag(event, topicId) {
    event.preventDefault();
    this.postIdFlag = topicId;
  }

  onFlagSend(event) {
    event.preventDefault();
    let data = new FormData();
    data.append("api_key", this.user.api_key);
    data.append("api_username", this.user.api_username);
    data.append("id", this.postIdFlag);
    data.append("post_action_type_id", this.actionSummaryId);
    data.append("flag_topic", "true");

    this._httpService.sendPostLike(this.user, data).subscribe(
      (data) => {
        this.sendPostLike = data;
        this.TopicLoader(this.user, this.topicId);
      }
    );
  }

  public onUnLike(event, topicId, topicActionsId) {
    event.preventDefault();
    this._httpService.sendPostUnLike(this.user, topicId, topicActionsId).subscribe(
      (data) => {
        this.sendPostLike = data;
        this.TopicLoader(this.user, this.topicId);
      }
    );
  }

  private getSelectionCoords(win) {
    win = win || window;
    let doc = win.document;
    let sel = doc.selection, range, rects, rect;
    let x = 0, y = 0;
    if (sel) {
      if (sel.type != "Control") {
        range = sel.createRange();
        range.collapse(true);
        x = range.boundingLeft;
        y = range.boundingTop;
      }
    } else if (win.getSelection) {
      sel = win.getSelection();
      if (sel.rangeCount) {
        range = sel.getRangeAt(0).cloneRange();
        if (range.getClientRects) {
          range.collapse(true);
          rects = range.getClientRects();
          if (rects.length > 0) {
            rect = rects[0];
          }
          x = rect.left;
          y = rect.top;
        }
        // Fall back to inserting a temporary element
        if (x == 0 && y == 0) {
          let span = doc.createElement("span");
          if (span.getClientRects) {
            // Ensure span has dimensions and position by
            // adding a zero-width space character
            span.appendChild(doc.createTextNode("\u200b"));
            range.insertNode(span);
            rect = span.getClientRects()[0];
            x = rect.left;
            y = rect.top;
            let spanParent = span.parentNode;
            spanParent.removeChild(span);

            // Glue any broken text nodes back together
            spanParent.normalize();
          }
        }
      }
    }
    return {x: x + 40, y: y};
  }

  onHighlight(event, topicText, topicId) {
    this.topicIdent = topicId;
    this.classIndex = false;
    this.textHighlight = document.getSelection().toString();
    this.coords = this.getSelectionCoords(window);
    this.coordsBlock.top = (this.coords.y + window.pageYOffset - 330).toString() + 'px';
    this.coordsBlock.left = (this.coords.x - 45).toString() + 'px';
    if (topicText.indexOf(this.textHighlight) && this.textHighlight.length > 0) {
      return this.classIndex = true;
    } else {
      return this.classIndex = false;
    }
  }

  open(content) {
    this.modalService.open(content).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  onShare (event, topicId) {
    event.preventDefault();
    this.postLink = document.location.href + '#post' + topicId;
    console.log(this.postLink);
  }

  public scrollTo (selector) {
    let Selector = selector;
    let hElement: HTMLElement = this.elRef.nativeElement;
    /*if ( Number(Selector.slice(2)) > 1) {
      document.querySelector(Selector).scrollIntoView();

    } else {
      window.scrollTo(0,0 );
    }*/

    let top = document.querySelector(Selector).offsetTop - ( window.innerHeight / 2 );
    console.log(top)
    window.scrollTo( 0, top + 235 );
    this.top = Number(this.SelectValue - 1) / (Number(this.maxValue) - 1 ) * 100;
  }

  LoginInContent(event) {
    event.preventDefault();
    this._loginService.LoginIn();
  }

  onScrollMobile(toggle) {
    this.showScroll = toggle;
  }

  previewVisibleOn(event) {
    event.preventDefault();
    this.previewVisible = !this.previewVisible;
  }
}
