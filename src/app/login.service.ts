/**
 * Created by astargh on 26.08.17.
 */

import { Injectable } from '@angular/core';
import * as crypto from 'crypto-js';
import { Http } from '@angular/http';
import { UserDataService } from './userdata.service';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/share';

@Injectable()

export class LoginService {

  private ssoKey: string = 'some_key_for_auth';

  constructor(public http: Http,
              public _userDataService: UserDataService ) {}

  public LoginIn() {
    let payload = 'nonce=lkljh345lkjl&return_sso_url=http://ec2-52-8-170-142.us-west-1.compute.amazonaws.com/#';
    let sso = btoa(payload);
    let sig = crypto.HmacSHA256(sso, this.ssoKey);
    sig = crypto.enc.Hex.stringify(sig);
    let urlRedir = this._userDataService.UserData.host + 'session/sso_provider?sso=' + sso + '&sig=' + sig;
    console.log(urlRedir);
    window.location.href = urlRedir;
  }
}
