
export class FilterList {
  public title: string;
  public bookmarked: boolean;
  public filterTitle: string;
  public filterTitle2: string;
  public active: boolean;
}

export const FILTERS: FilterList[] = [
  {
    'title' : 'Topics',
    'bookmarked': false,
    'filterTitle' : '-id',
    'filterTitle2' : 'id',
    'active': true
  },
  {
    'title' : 'Lastest activity',
    'bookmarked': false,
    'filterTitle' : '-last_posted_at',
    'filterTitle2' : 'last_posted_at',
    'active': true
  },
  {
    'title' : 'My Subscriptions',
    'bookmarked': true,
    'filterTitle' : '-highest_post_number',
    'filterTitle2' : 'highest_post_number',
    'active': false
  },
  {
    'title' : 'Hot',
    'bookmarked': false,
    'filterTitle' : '-views',
    'filterTitle2' : 'views',
    'active': true
  },
];
