import { Injectable } from '@angular/core';
import { User } from './user';

Injectable();
export class UserDataService {

  public UserData: User = {
    host: 'http://forums.wardragons.com/',
    api_key: '94ad1b932040e91fe977c00d7d4a67f84d062e33c1c38cdcf75d3fb4b1543295',
    api_username: 'system'
  };

  public getUserData(): User {
    if ( localStorage.getItem('username') != null) {
      this.UserData.api_username = localStorage.getItem('username');
    }
    return this.UserData;
  }

}
