import { Component, OnInit } from '@angular/core';
import { HomeService } from './home.service';
import { Title } from './title';
import { ModalService } from '../modal/modal.service';
import { LoginService } from '../login.service';
import { UserDataService } from '../userdata.service';

@Component({
  selector: 'home',
  providers: [
    Title,
    HomeService,
    ModalService
  ],
  styleUrls: [ './home.component.scss' ],
  templateUrl: './home.component.html',
})

export class HomeComponent implements OnInit {
  public getData: object;
  public userData;
  public logoTop = 'assets/img/lockup.png';
  public UserData;

  constructor(private _httpService: HomeService,
              private _loginService: LoginService,
              private _modalService: ModalService,
              private _userDataService: UserDataService ) {
  }

  ngOnInit() {
    this.userData = this._userDataService.getUserData();
    if (this.userData.api_username != null) {
      this.currentUser(this.userData);
    }
    this.getCategoryList(this.userData);
  }

  getCategoryList ( userdata ) {
    this._httpService.getAjaxObject( userdata ).subscribe(
      data => {
        this.getData = data.category_list.categories;
        console.log(this.getData);
      }
    );
  }

  LoginInContent(event) {
    event.preventDefault();
    this._loginService.LoginIn();
  }

  currentUser( userdata ) {
    this._modalService.makeAjaxRequest( userdata ).subscribe(
      data => {
        if ( data == null) {
          console.log('null');
        } else {
          this.UserData = data;
        }

      }
    );
  }

}
