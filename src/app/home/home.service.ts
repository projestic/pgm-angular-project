/**
 * Created by Astargh on 06.07.2017.
 */

import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class HomeService {
  constructor(public http: Http) {
  }

  getAjaxObject( userdata ) {
    /*if ( userName == '' || userName == null) {
      userName = 'anonymous';
      apiKey = 'bd14a497e4efc11dd138d9972e18105f25c04818c1dbc0680600e5097d141405'
    }*/
    return this.http.get(userdata.host + '/categories.json?api_key=' + userdata.api_key + '&api_username=' + userdata.api_username)
      .map(res => res.json());
  }
}
