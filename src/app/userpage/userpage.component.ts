import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { User } from '../user';
import { UserDataService } from '../userdata.service';
import { UserpageService } from './userpage.service';

@Component ({
  selector: 'userpage',
  providers: [UserpageService],
  styleUrls: [ './userpage.component.scss' ],
  templateUrl: './userpage.component.html',
})


export class UserpageComponent implements OnInit {
  public userData: User;
  public userGetData;
  constructor ( private _route: ActivatedRoute,
                private _userData: UserDataService,
                private _userAjax: UserpageService ) {}

  ngOnInit () {
    this.userData = this._userData.getUserData();
    let username = this._route.snapshot.params['id'];

    this.getUserInfo(this.userData, username);
  }

  public getUserInfo(userData, username) {
    this._userAjax.getAjaxUserObject( userData, username ).subscribe(
      data => {
        this.userGetData = data;
        console.log(this.userGetData);
      }
    );
  }
}
