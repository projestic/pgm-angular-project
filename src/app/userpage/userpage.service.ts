import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()

export class UserpageService {
  constructor ( public http: Http ) {}

  public getAjaxUserObject (userData, username) {
    if ( userData.api_username == '') {
      userData.api_username = 'anonymous';
      userData.api_key = '4258805b4d404cf7a12756f5f99fca4f62bf6f320a45b6190bf675ab78a8b58b'
    }
    return this.http.get('http://dogwelder.pocketgems.com/users/'
      + username + '.json?api_key=' + userData.api_key +
      '&api_username=' + userData.api_username)
      .map(res => res.json());
  }
}
