/**
 * Created by Astargh on 06.07.2017.
 */

import {Injectable, Input} from '@angular/core';
import {HomeService} from "../home/home.service";
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class CategoryService extends HomeService {
  @Input() apiLink;

  getAjaxObjectCat(apiLink) {
    return this.http.get(apiLink)
      .map(res => res.json());
  }

  getUserListRequest(userdata, useritem) {
    return this.http.get(userdata.host + 'users/' + useritem.username + '.json?api_key=' + userdata.api_key + '&api_username=' + userdata.api_username)
      .map(res => res.json());
  }
}
