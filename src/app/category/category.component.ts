import { Component, Input, OnInit, HostListener } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CategoryService } from './category.service';
import { ModalService } from '../modal/modal.service';
import { FILTERS } from '../filter-obj';
import { UserDataService } from '../userdata.service';

@Component({
  selector: 'category',
  providers: [ CategoryService, ModalService ],
  styleUrls: [`./category.component.scss`],
  templateUrl: 'category.component.html',
})

export class CategoryComponent implements OnInit {

  public departmentId;
  public apiLink;
  public infoName;
  public infoSlug;
  public infoImg;
  public infoId;
  public filterTitle;
  public filterTitle2;
  public filterargs: object;
  public userData;
  public newDataCat;
  public userList: Array<object> = [];
  public selectedItem;
  public getData: object;
  public getDataCat: object;
  public infos: any;
  public UserName;
  public filters = FILTERS;
  public error;
  public userDataAjax;
  public topicList = [];
  public direction = '';
  public sum = -1;
  public pages;
  public loadingInProgress = false;
  public topicNumber;
  public newTopicNumber;

  constructor(private _httpService: CategoryService,
              private route: ActivatedRoute,
              private ModalService: ModalService,
              private _userData: UserDataService) {}

  @HostListener("window:scroll", [])

  onWindowScroll() {
    const offset = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;
    const screen = window.innerHeight;
    const bodyHeight = document.body.offsetHeight;
    if (bodyHeight - offset == screen ) {
      this.onScrollDown(this.pages);
    }
  }

  public ngOnInit() {
    this.userData = this._userData.getUserData();


    this.currentUser( this.userData );
    this.getCategoryList( this.userData );
    this.newListRequest();

    let id = this.route.snapshot.params['id'];

    this.departmentId = id;

    this.filterTitle = 'category_id';

    /*this.getUserList();*/

    this.filterTitle = '-last_posted_at';
    this.filterTitle2 = 'last_posted_at';

  }

  public getCategoryList( userdata ) {

    this._httpService.getAjaxObject( userdata ).subscribe(
      data => {
        this.getData = data;
        this.infos = data.category_list.categories;

        for ( let info of this.infos ){
          if (info.name == this.departmentId) {
            this.infoName = info.name;
            this.infoSlug = info.slug;
            this.infoId = info.id;
            this.topicNumber = info.topic_count;
            this.newTopicNumber = info.topic_count
            if ( info.uploaded_logo !== null ) {
              this.infoImg = info.uploaded_logo.url;
            }
            this.pages = Math.ceil(info.topic_count / 30 );

            this.apiLink = userdata.host + "c/" + this.infoId + ".json?api_key=" + userdata.api_key + "&api_username="  + userdata.api_username;


            this.onScrollDown(this.pages);

          }
        }
      },
      err => {
        this.error = err;
      }
    );
  }

  public currentUser(username) {
    this.ModalService.makeAjaxRequest(username).subscribe(
      data => {
        if ( data == null) {
          console.log('null');
        } else {
          this.userDataAjax = data;
        }

      }
    );
  }

  public newListRequest() {
    setInterval( () => {
      this._httpService.getAjaxObject( this.userData ).subscribe(
        data => {
          this.getData = data;
          this.infos = data.category_list.categories;

          for ( let info of this.infos ){
            if (info.name == this.departmentId ) {
              this.newTopicNumber = info.topic_count;
            }
          }
        },
        err => {
          this.error = err;
        }
      );
    }, 20000);
  }

  public refreshList( catId ) {
    this.onScrollDown(this.pages = 1);
  }

  //filter function

  public onClickLatest(event, bookmarked, title, title2, newValue) {
    event.preventDefault();
    this.filterargs = { 'bookmarked': bookmarked };
    this.selectedItem = newValue;
    this.filterTitle = title;
    this.filterTitle2 = -title2;

  }

  // getting topic object at page loading and scroll down

  public onScrollDown (pages) {
    if (+this.sum < +pages ) {
      this.loadingInProgress = true;
      this.sum += 1;
      if ( pages == 1 ) {
        this.apiLink = this.userData.host + 'c/' + this.infoId + '.json?api_key=' + this.userData.api_key + '&api_username='  + this.userData.api_username;
      } else {
        this.apiLink = this.userData.host + 'c/' + this.infoId + '.json?page=' + this.sum + '&api_key=' + this.userData.api_key + '&api_username='  + this.userData.api_username;
      }
      this._httpService.getAjaxObjectCat( this.apiLink ).subscribe(
        data => {
          this.getDataCat = data;
          this.topicList = this.topicList.concat(this.getDataCat['topic_list'].topics);
          console.log(this.sum, pages, this.topicList);
          for (let userItem of this.getDataCat['users'] ) {
            this._httpService.getUserListRequest(this.userData, userItem).subscribe(
              data => {
                if ( this.userList.findIndex(x => x['user'].id == data.user.id) == -1 ) {
                  this.userList.push(data);
                  console.log(this.userList);
                }

                setTimeout( () => {
                  this.loadingInProgress = false;
                }, 5000);
              }
            );
          }
        }
      );
    }
  }

  // choose icoon bettween admin and not admin

  public imageVerif (userItem, adminUser) {
    if (userItem.extras != null && userItem.extras.indexOf('latest') != -1 && userItem.user_id == adminUser.user.id && adminUser.user.admin == true){ return true }
    if (userItem.extras != null && userItem.extras.indexOf('latest') != -1 && userItem.user_id == adminUser.user.id && adminUser.user.admin == false){ return false }
  }


}

